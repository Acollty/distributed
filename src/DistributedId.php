<?php

/**
 * Class DistributedId
 * 雪花算法
 * 分布式 id 生成类     组成: <毫秒级时间戳+机器id+序列号>
 * 默认情况下41bit的时间戳可以支持该算法使用到2082年，10bit的工作机器id可以支持1023台机器，序列号支持1毫秒产生4095个自增序列id
 * 最高位是符号位，始终为0，不可用。
 * 41位的时间序列，精确到毫秒级，41位的长度可以使用69年。时间位还有一个很重要的作用是可以根据时间进行排序。
 * 10位的机器标识，10位的长度最多支持部署1024个节点。
 * 12位的计数序列号，序列号即一系列的自增id，可以支持同一节点同一毫秒生成多个ID序号，12位的计数序列号支持每个节点每毫秒产生4096个ID序号。
 * 看的出来，这个算法很简洁也很简单，但依旧是一个很好的ID生成策略。其中，10位器标识符一般是5位IDC+5位machine编号，唯一确定一台机器。
 */
class DistributedId
{
    const EPOCH = 1479533469598;    //开始时间,固定一个小于当前时间的毫秒数
    const max12bit = 4095;
    const max41bit = 1099511627775;
    static $machineId = null;      // 机器id

    /**
     * @param int $machineId
     */
    public static function machineId($machineId = 0){
        self::$machineId = $machineId;
    }

    /**
     *
     */
    public static function createId(){
        $time = floor(microtime(true)*1000);//时间戳42字节
        $time -= self::EPOCH; // 当前时间 与 开始时间 差值
        $base = decbin(self::max41bit + $time);// 二进制的 毫秒级时间戳
        // 机器id  10 字节
        if(!self::$machineId)
        {
            $machineId = self::$machineId;
        }
        else
        {
            $machineId = str_pad(decbin(self::$machineId), 10, "0", STR_PAD_LEFT);
        }
        $random = str_pad(decbin(mt_rand(0, self::max12bit)), 12, "0", STR_PAD_LEFT);
        // 拼接
        $base = $base.$machineId.$random;
        // 转化为 十进制 返回
        return bindec($base);
    }
}

//调用方式
//DistributedId::machineId('91acb851-7909-474d-9999-3365d480a6f1');//机器编号
//DistributedId::createId();//分布式ID

